import  math
'''
This is a simple example for quadratic equation
a*x**2+b*x +c=0
'''

#calculation methode
def calculation(a, b, c):
 result_x1 = (-b+math.sqrt(b**2-4*a*c))/(2*a)
 result_x2 = (-b-math.sqrt(b**2-4*a*c))/(2*a)
 return result_x1, result_x2

#main methode


def main():
 a = int(input("Input a value: "))
 b = int(input("Input b value: "))
 c = int(input("Input c value: "))
 result = calculation(a, b, c)
 print("The results for x1={:.2f} and x2={:.2f}".format(result[0], result[1]))



if __name__ == '__main__':
    main()